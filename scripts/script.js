const containerCards = document.querySelector('.gallery');
const challenges = [
    "DIENORAŠTIS<br><hr>Daugiausia dėmesio skirk emocijoms",
    "DIENORAŠTIS<br><hr>Daugiausia dėmesio skirk emocijoms",
    "DIENORAŠTIS<br><hr>Daugiausia dėmesio skirk emocijoms",
    "DIENORAŠTIS<br><hr>Daugiausia dėmesio skirk emocijoms",
    "DIENORAŠTIS<br><hr>Daugiausia dėmesio skirk emocijoms",
    "DIENORAŠTIS<br><hr>Daugiausia dėmesio skirk emocijoms",
    "ATASKAITA<br><hr>atnaujinti tik svorį bezalos.lt",
    "UŽKANDŽIAI<br><hr>Iš vakaro susiplanuok, kitą dieną nufotografuok",
    "UŽKANDŽIAI<br><hr>Iš vakaro susiplanuok, kitą dieną nufotografuok",
    "UŽKANDŽIAI<br><hr>Iš vakaro susiplanuok, kitą dieną nufotografuok",
    "UŽKANDŽIAI<br><hr>Iš vakaro susiplanuok, kitą dieną nufotografuok",
    "UŽKANDŽIAI<br><hr>Iš vakaro susiplanuok, kitą dieną nufotografuok",
    "UŽKANDŽIAI<br><hr>Iš vakaro susiplanuok, kitą dieną nufotografuok",
    "ATASKAITA<br><hr>atnaujinti svorį ir apimtis bezalos.lt",
    "MAISTO PLANAVIMAS<br><hr>Mityba be perdirbtų maisto produktų",
    "MAISTO PLANAVIMAS<br><hr>Mityba be perdirbtų maisto produktų",
    "MAISTO PLANAVIMAS<br><hr>Mityba be perdirbtų maisto produktų",
    "MAISTO PLANAVIMAS<br><hr>Mityba be perdirbtų maisto produktų",
    "MAISTO PLANAVIMAS<br><hr>Mityba be perdirbtų maisto produktų",
    "MAISTO PLANAVIMAS<br><hr>Mityba be perdirbtų maisto produktų",
    "ATASKAITA<br><hr>atnaujinti tik svorį bezalos.lt",
    "MAISTO FOTO<br><hr>Visos dienos maisto nuotraukų koliažas",
    "MAISTO FOTO<br><hr>Visos dienos maisto nuotraukų koliažas",
    "MAISTO FOTO<br><hr>Visos dienos maisto nuotraukų koliažas",
    "MAISTO FOTO<br><hr>Visos dienos maisto nuotraukų koliažas",
    "MAISTO FOTO<br><hr>Visos dienos maisto nuotraukų koliažas",
    "MAISTO FOTO<br><hr>Visos dienos maisto nuotraukų koliažas",
    "ATASKAITA<br><hr>atnaujinti svorį, apimtis ir nuotraukas bezalos.lt"
];
let day = 30;
let month = 1;
const CARDS = [];

for (var i = 1; i <= 28; i++) {

    CARDS.push({
        id: i,
        day: moment(`2023-${month}-${day}`, 'YYYY-MM-DD').format('YYYY-MM-DD'),
        challenge: challenges[i - 1]
    });
    day++;
    if(day > 31) {
        day = 1;
        month = 2;
    }
}
    
CARDS.forEach(function(card, i) {
    let day = moment(card.day, 'YYYY-MM-DD').diff(moment().format('YYYY-MM-DD'));

    const html = `<div class="flip_card ${day < 0 ? 'flipp' : ''}" data-id=${card.id} data-day=${card.day}>
        <div class="top">${i+1}.</div>
        <div class="bottom" id="_${i+1}">
            <span class="bottom-span">${i+1}.</span>
            <span class="challenge">${day <= 0 ? card.challenge : ''}</span>
        </div>
    </div>`;

    // const html_test_icon = `<div class="flip_card ${flipp}" data-id=${card.id} data-day=${card.day}>
	// 	<div class="top">${i+1}.</div>
	// 	<div class="bottom" id="_${i+1}">
	// 		<span class="bottom-span">${i+1}.</span>
	// 		<span class="challenge">${card.challenge}</span>
	// 		<div class="test">
	// 			<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
	//   				<path stroke-linecap="round" stroke-linejoin="round" d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
	// 			</svg>
	// 		</div>
	// 	</div>
	// </div>`;

    containerCards.insertAdjacentHTML('beforeend', html);
});

const cards = document.querySelectorAll('.flip_card');
cards.forEach(el => el.addEventListener("click", function(e) {

    if (moment(el.dataset.day, "YYYY-MM-DD").diff(moment().format("YYYY-MM-DD")) <= 0) {
        const id = el.dataset.id;
        el.classList.toggle('flipp')
    } else if (moment(el.dataset.day, "YYYY-MM-DD").diff(moment().format("YYYY-MM-DD")) > 0) {
        el.classList.add('horizontal-shake');
        setTimeout(() => {
            el.classList.remove('horizontal-shake');
        }, 500);
    }
}));