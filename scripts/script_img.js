const containerCards = document.querySelector('.gallery');
const CARDS = [];

for(var i = 1; i <= 24; i++) {
	CARDS.push({
		id: i,
		day: moment().format(`2022-12-${i}`),
		url_front: `images/${i}_front.jpg`,
		url_back: `images/${i}_back.jpg`,
	});
}


CARDS.forEach(function(card, i) {
	let image_back_url = "images/hide.jpg";
	let flipp = "";
	if(moment(card.day, "YYYY-MM-DD").diff(moment().format("YYYY-MM-DD")) < 0) {
		flipp = "flipp";
		image_back_url = card.url_back;
	} 
	const html = `<div class="flip_card ${flipp}" data-id=${card.id} data-day=${card.day}>
		<img class="top" src="${card.url_front}" alt="">
		<img class="bottom" id="_${i+1}" src="${image_back_url}" alt="">
	</div>`;

	// const html = `<div class="flip_card ${flipp}" data-id=${card.id} data-day=${card.day}>
	// 	<p class="top" style="color: #003333">TOP</p>
	// 	<p class="bottom" id="_${i+1}" style="color: #003333">Bottom</p>
	// </div>`;
	
	containerCards.insertAdjacentHTML('beforeend', html);
});

const cards = document.querySelectorAll('.flip_card');
cards.forEach(el => el.addEventListener("click", function(e) {
	
	if(moment(el.dataset.day, "YYYY-MM-DD").diff(moment().format("YYYY-MM-DD")) <= 0) {
		const id = el.dataset.id;

		const img_src = document.querySelector(`#_${id}`).src.replace('hide', `${id}_back`);
		document.querySelector(`#_${id}`).src = img_src;

		el.classList.toggle('flipp')

	} else if(moment(el.dataset.day, "YYYY-MM-DD").diff(moment().format("YYYY-MM-DD")) > 0) {
		el.classList.add('horizontal-shake');
		setTimeout(() => {
			el.classList.remove('horizontal-shake');
		}, 500);
	}
}));


